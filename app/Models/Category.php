<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\category\CategoryPresenter as webCategoryPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Category extends Model
{
    use HasFactory, Presentable;

    public $webPresenter = webCategoryPresenter::class;

    protected $guarded = [
        'id'
    ];

    public function commodities(): MorphToMany
    {
        return $this->morphedByMany(Commodity::class,
            'entity',
            'categorizable',
            'category_id',
            'entity_id',
            'id',
            'id'
        );
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }
}
