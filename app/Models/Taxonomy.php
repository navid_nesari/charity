<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Taxonomy\TaxonomyPresenter as webTaxonomyPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Taxonomy extends Model
{
    use HasFactory, Presentable;

    public $webPresenter = webTaxonomyPresenter::class;


    protected $guarded = [
        'id',
    ];

    public function investigators(): MorphToMany
    {
        return $this->morphedByMany(Investigator::class,
            'entity',
            'taxonomiables',
            'taxonomy_id',
            'entity_id',
            'id',
            'id'
        );
    }
}
