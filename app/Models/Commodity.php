<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Commodity\CommodityPresenter as webCommodityPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Commodity extends Model
{
    use HasFactory, Presentable;

    public $webPresenter = webCommodityPresenter::class;

    public function getRouteKeyName() {
        return 'id';
      }


    protected $guarded = [
        'id'
    ];

    public function categories(): MorphToMany
    {
//        dd($this->morphToMany(Taxonomy::class, 'entity','taxonomiables', 'entity_id',
//            'taxonomy_id',
//            'id',
//            'id',
//            false
//        ));
        return $this->morphToMany(Category::class,
            'entity',
            'categorizable',
            'entity_id',
            'category_id',
            'id',
            'id'
        );
    }

}
