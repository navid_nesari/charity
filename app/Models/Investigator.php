<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Investigator\InvestigatorPresenter as webInvestigatorPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Investigator extends Model
{
    use HasFactory, Presentable;

    public $webPresenter = webInvestigatorPresenter::class;


    protected $guarded = [
        'id',
    ];

    // for handel ARRAY TO STRING CONVERT when create or update data.
    protected $casts = [
        'needies_types' => 'array',
        'houses_types' => 'array',
        'identification_docs_types' => 'array',
    ];

    public function taxonomies(): MorphToMany
    {
//        dd($this->morphToMany(Taxonomy::class, 'entity','taxonomiables', 'entity_id',
//            'taxonomy_id',
//            'id',
//            'id',
//            false
//        ));
        return $this->morphToMany(Taxonomy::class,
            'entity',
            'taxonomiables',
            'entity_id',
            'taxonomy_id',
            'id',
            'id'
        );
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
