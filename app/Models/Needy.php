<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Needy extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }

//    public function help(): HasOne
//    {
//        return $this->hasOne(Help::class);
//    }

    public function helps(): MorphToMany
    {
        return $this->morphToMany(Help::class,
            'entity',
            'help_details',
            'entity_id',
            'help_id',
            'id',
            'id'
        );
    }

    public function children()
    {
        return $this->hasMany(Needy::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Needy::class, 'parent_id', 'id');
    }
}
