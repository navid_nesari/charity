<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Admin\AdminPresenter as webAdminPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use HasFactory, Presentable;

    public $webPresenter = webAdminPresenter::class;

    protected $guarded = [
        'id',
    ];
}
