<?php

namespace App\Presenters\Web\Taxonomy;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class TaxonomyPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == Constant::IN_ACTIVE) {
            return "<span class='badge badge-light-danger badge-sm px-2'>غیر فعال</span>";
        }else {
            return "<span class='badge badge-light-success badge-sm px-2'>فعال</span>";
        }
    }
}