<?php

namespace App\Presenters\Web\Investigator;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class InvestigatorPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == Constant::REJECTED) {
            return "<span class='badge badge-light-danger badge-sm px-2'>رد شده</span>";
        } else {
            return "<span class='badge badge-light-success badge-sm px-2'>تایید شده</span>";
        }
    }
}