<?php

namespace App\Presenters\Web\Commodity;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class CommodityPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == Constant::IN_ACTIVE) {
            return "<span class='badge badge-light-danger badge-sm px-2'>غیر فعال</span>";
        }
        return "<span class='badge badge-light-success badge-sm px-2'>فعال</span>";
    }
}