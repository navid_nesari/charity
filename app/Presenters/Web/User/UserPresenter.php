<?php

namespace App\Presenters\Web\User;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class UserPresenter extends Presenter
{
    public function status()
    {
        if ($this->entity->status == Constant::IN_ACTIVE) {
            return "<span class='badge badge-light-danger'>غیر فعال</span>";
        }
        return "<span class='badge badge-light-success badge-sm px-2'>فعال</span>";
    }

    public function avatar()
    {
        if ( is_null($this->entity->avatar) || $this->entity->avatar == '' ) {
            return asset('admin-assets/media/avatars/blank.png');
        }
        return str_replace('\\', '/', asset(Constant::USER_IMAGE_PATH . $this->entity->avatar));
    }
}