<?php

namespace App\Constants;

class Constant
{
    // Global Constant
    const ACTIVE = 'active';
    const IN_ACTIVE = 'in-active';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';


    const IMAGE_TYPE = 'image';
    const VIDEO_TYPE = 'video';



    const UN_DEFINED = "undefined";


    // taxonomy entities
    const PLAN = 'plan';
    const SERVICE = 'service';
    const IDENTIFICATION_DOCUMENT = 'identification-document';
    const HOUSE = 'house';
    const HOUSEHOLD = 'household';

    // needies type
    const SADAT = 'sadat';
    const ORDINARY = 'ordinary';
    const ORPHANS = 'orphans';
    const MARTYR = 'martyr';
    const SUNNI = 'sunni';


    // identification document
    const CARD = 'card';
    const STAY = 'stay';
    const WITHOUT_PROOF = 'without-proof';
    const IDENTIFIED = 'identified';


    // house type
    const RENT = 'rent';
    const MORTGAGE = 'mortgage';
    const PERSONAL = 'personal';
    const FREE = 'free';

    // categories entity
    const COMMODITY = 'commodity';




    // ADMIN uploads path
    const ADMIN_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'admins' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    // USER uploads path
    const USER_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';


    public static function getStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::ACTIVE => 'فعال',
                Constant::IN_ACTIVE => ' غیر فعال',
            ];
        }
        if (in_array($status, array_keys(self::getStatuses()))) {
            return self::getStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }
    public static function getStatusesViewer()
    {
        $activeStatuses = self::getStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value,
            ];
        }
        return $activeStatusesViewer;
    }

    public static function getTaxonomiesEntities($entity = null)
    {
        if (is_null($entity)) {
            return [
                Constant::PLAN => 'کمک ها',
                Constant::SERVICE => 'خدمات',
                Constant::IDENTIFICATION_DOCUMENT => 'مداراک شناسایی',
                Constant::HOUSE => 'خانه',
                Constant::HOUSEHOLD => 'نوع خانواده',
            ];
        }
        if (in_array($entity, array_keys(self::getTaxonomiesEntities()))) {
            return self::getTaxonomiesEntities()[$entity];
        }
        return Constant::UN_DEFINED;
    }
    public static function getTaxonomiesEntitiesViewer()
    {
        $getTaxonomiesEntities = self::getTaxonomiesEntities();
        $getTaxonomiesEntitiesViewer = [];
        foreach ($getTaxonomiesEntities as $key => $value) {
            $getTaxonomiesEntitiesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $getTaxonomiesEntitiesViewer;
    }


    public static function getInvestigatorStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::APPROVED => 'تایید شده',
                Constant::REJECTED => 'رد شده',
            ];
        }
        if (in_array($status, array_keys(self::getInvestigatorStatuses()))) {
            return self::getInvestigatorStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }
    public static function getInvestigatorStatusesViewer()
    {
        $activeStatuses = self::getInvestigatorStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }


    public static function getNeediesTypes($type = null)
    {
        if (is_null($type)) {
            return [
                Constant::SADAT => 'سادات',
                Constant::ORDINARY => 'عام',
                Constant::ORPHANS => 'ایتام',
                Constant::MARTYR => 'شهید',
                Constant::SUNNI => 'اهل تسنن',
            ];
        }
        if (in_array($type, array_keys(self::getNeediesTypes()))) {
            return self::getNeediesTypes()[$type];
        }
        return Constant::UN_DEFINED;
    }
    public static function getNeediesTypesViewer()
    {
        $getNeediesTypes = self::getNeediesTypes();
        $getNeediesTypesViewer = [];
        foreach ($getNeediesTypes as $key => $value) {
            $getNeediesTypesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $getNeediesTypesViewer;
    }


    public static function getIdentificationDocsTypes($document = null)
    {
        if (is_null($document)) {
            return[
              Constant::CARD => 'کارت',
              Constant::STAY => 'اقامت',
              Constant::WITHOUT_PROOF => 'بدون مدرک',
              Constant::IDENTIFIED => 'شناسایی',
            ];
        }
        if (in_array($document, array_keys(self::getIdentificationDocsTypes()))) {
            return self::getIdentificationDocsTypes()[$document];
        }
        return Constant::UN_DEFINED;
    }
    public static function getIdentificationDocsTypesViewer()
    {
        $getIdentificationDocsTypes = self::getIdentificationDocsTypes();
        $getIdentificationDocsTypesViewer = [];
        foreach ($getIdentificationDocsTypes as $key => $value) {
            $getIdentificationDocsTypesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $getIdentificationDocsTypesViewer;
    }


    public static function getHousesTypes($type = null)
    {
        if (is_null($type)) {
            return [
                Constant::RENT => 'اجاره',
                Constant::MORTGAGE => 'رهن',
                Constant::PERSONAL => 'شخصی',
                Constant::FREE => 'رایگان',
            ];
        }
        if (in_array($type, array_keys(self::getHousesTypes()))) {
            return self::getHousesTypes()[$type];
        }
        return Constant::UN_DEFINED;
    }
    public static function getHousesTypesViewer()
    {
        $getHousesTypes = self::getHousesTypes();
        $getHousesTypesViewer = [];
        foreach ($getHousesTypes as $key => $value) {
            $getHousesTypesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $getHousesTypesViewer;
    }


    // category entities
    public static function getCategoryEntities($status = null)
    {
        $categoryEntities = [
            self::COMMODITY => 'اقلام کمکی',
        ];
        if (is_null($status)) {
            return $categoryEntities;
        }
        if (in_array($status, array_keys($categoryEntities))) {
            return $categoryEntities[$status];
        }
    }

    public static function getCategoryEntitiesViewer(): array
    {
        $categoryEntities = self::getCategoryEntities();
        $categoryEntitiesViewer = [];
        foreach ($categoryEntities as $key => $value) {
            $categoryEntitiesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $categoryEntitiesViewer;
    }
}


