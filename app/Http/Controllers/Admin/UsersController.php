<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::latest()->paginate();

        return view('admin.users.all', compact('users'));
    }


    public function delete(User $user)
    {
        $user->delete();

        return redirect()->back();
    }
}
