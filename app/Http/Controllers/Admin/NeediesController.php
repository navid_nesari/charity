<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Needy;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class NeediesController extends BaseController
{
    public function familiesHeadAll()
    {
        $needies = Needy::latest()->paginate()->groupBy('parent_id');

        if(isset($needies['']) && $needies[""]->count() > 0) {
            $needies['root'] = $needies[''];
            // delete extra data
            unset($needies['']);
        }

        return view('admin.needies.all-family-head', compact('needies'));
    }


    // CRUD operation
    public function create()
    {
        return view('admin.needies.create');

    }


    public function store()
    {
        $userData = [];
        // user


        $needyData = [];
        // needy


    }






    public function familiesMemberCreate(Needy $needy)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.needies.family-head.create', compact('needy', 'statuses'));
    }


    public function familiesMemberStore(Needy $needy)
    {
        request()->validate([
           'first_name' =>['required'],
           'last_name' =>['required'],
           'mobile' =>['nullable','unique:users,mobile','min:11','numeric'],
           'birth_date' =>['nullable'],
           'approved_date' =>['required'],
           'code_melli' =>['required','unique:users,code_melli'],
           'address' =>['required'],
           'status' =>['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
           'avatar' =>['nullable', 'mimes:jpeg,jpg,png,bmp'],
        ]);

        $userData = [
            'investigator_id' => $needy->user->investigator_id,
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'mobile' => request('mobile'),
            'birth_date' =>  Date::toCarbonDateFormat(request('birth_date')),
            'code_melli' => request('code_melli'),
            'address' => request('address'),
            'status' => request('status'),
        ];

        if ( request('avatar') ) {
            $userData['avatar'] = $this->uploadImage(request('avatar'), Constant::USER_IMAGE_PATH);
        }

        // user
        $user = User::create($userData);

        // needy
        $needyData = [
            'parent_id' => $needy->id,
            'user_id' => $user->id,
            'approved_date' => Date::toCarbonDateFormat(request('approved_date')),
        ];
        Needy::create($needyData);

        return redirect()->route('admin.needies.families.head.all', ['needy' => $needy]);
    }


    public function familiesMemberEdit(Needy $needy)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.needies.family-head.edit', compact('needy', 'statuses'));
    }


    public function familiesMemberUpdate(Needy $needy)
    {
        //validation
        request()->validate([
            'first_name' =>['required'],
            'last_name' =>['required'],
            'mobile' =>['nullable','min:11','numeric',Rule::unique('users')->ignore($needy->user->id)],
            'birth_date' =>['nullable'],
            'approved_date' =>['required'],
            'code_melli' =>['required',Rule::unique('users')->ignore($needy->user->id)],
            'address' =>['required'],
            'status' =>['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
            'avatar' =>['nullable', 'mimes:jpeg,jpg,png,bmp'],
        ]);

        // get data
        $userData = [
            'investigator_id' => $needy->user->investigator_id,
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'mobile' => request('mobile'),
            'birth_date' =>  Date::toCarbonDateFormat(request('birth_date')),
            'code_melli' => request('code_melli'),
            'address' => request('address'),
            'status' => request('status'),
        ];

        if ( request('avatar') ) {
            $userData['avatar'] = $this->uploadImage(request('avatar'), Constant::USER_IMAGE_PATH);
        }

        // user
        $needy->user->update($userData);

        // needy
        $needyData = [
            'parent_id' => $needy->parent->id,
            'user_id' => $needy->user->id,
            'approved_date' => Date::toCarbonDateFormat(request('approved_date')),
        ];
        $needy->update($needyData);

        return redirect()->route('admin.needies.families.head.all', ['needy' => $needy]);
    }


    public function delete(Needy $needy)
    {
        $needy->delete();

        return redirect()->back();
    }
}
