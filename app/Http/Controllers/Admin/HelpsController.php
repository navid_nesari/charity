<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\Controller;
use App\Models\Commodity;
use App\Models\Help;
use App\Models\HelpDetails;
use App\Models\Needy;
use App\Models\User;
use Illuminate\Http\Request;

class HelpsController extends Controller
{

    public function __construct()
    {
        if( request('needy') == null){
            abort(404);
        }
    }



    public function all(string $needy)
    {
         $needyParent = Needy::where('id', $needy)->first();
         $userId = $needyParent->user_id;
//        $needyParent = Needy::where('id', $needy)->get()->pluck('user_id')->toArray();
//        $userId = $needyParent[0];
        $user = User::where('id', $userId)->first();

        $helps = Help::latest()->paginate();

        return view('admin.helps.all', compact('helps', 'needy', 'user'));
    }



    public function store(string $needy)
    {
        //validate
        request()->validate([
            'title' => ['required'],
            'done_at' => ['required'],
            'description' => ['required', 'string'],
            'needy_hidden' => ['required', 'numeric']
        ],[
                'title.required' => 'فیلد مورد نظر الزامی است.',
                'description.string' => '',
        ]);

        if ( !$this->isValidNeedy() ) {
            return redirect()->route('admin.helps.all', ['needy' => $needy]);
        }

        $data = [
            'needy_id' => $needy,
            'title' => request('title'),
            'done_at' => Date::toCarbonDateFormat( request('done_at') ),
            'description' => request('description'),
        ];
        $help = Help::create($data);

        if( $help instanceof Help ){
            return redirect()->route('admin.helps.all', ['needy' => $needy]);
        }

        return redirect()->route('admin.helps.all', ['needy' => $needy]);
    }


    public function edit(string $needy, Help $help)
    {
        return view('admin.helps.edit', compact('needy', 'help'));
    }


    public function update(string $needy, Help $help)
    {
        //validate
        request()->validate([
            'title' => ['required'],
            'done_at' => ['required'],
            'description' => ['required', 'string'],
            'needy_hidden' => ['required', 'numeric']
        ],[
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
        ]);

        if ( !$this->isValidNeedy() ) {
            return redirect()->route('admin.helps.all', ['needy' => $needy]);
        }
//dd(\request()->all());
        $data = [
            'needy_id' => $needy,
            'title' => request('title'),
            'done_at' => Date::toCarbonDateFormat( request('done_at') ),
            'description' => request('description'),
        ];
        $help->update($data);

        return redirect()->route('admin.helps.all', ['needy' => $needy]);
    }



    public function delete(string $needy, Help $help)
    {
        $help->delete();

        return redirect()->route('admin.helps.all', ['needy' => $needy]);
    }



    public function isValidNeedy()
    {
        if ( request('needy') == request('needy_hidden')) {
            return true;
        }
    }



}
