<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\JsTree;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Commodity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CommoditiesController extends Controller
{
    public function index()
    {
        $commodities = Commodity::latest()->paginate();

        return view('admin.commodities.all', compact('commodities'));
    }


    public function create()
    {
       $statuses = Constant::getStatusesViewer();
       $categories = JsTree::getDataFor(Category::class, 'parent_id');

        return view('admin.commodities.create', compact('statuses', 'categories'));
    }


    public function store()
    {
        request()->validate([
            'title' => ['required'],
            'stock' => ['required'],
            'unit' => ['required'],
            'description' => ['nullable', 'string'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
            'categories' => ['required']
        ],
            [
                'title.required' => 'فیلد مورد نظر الزامی است.',
                'description.string' => '',
            ]);

        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'stock' => request('stock'),
            'unit' => request('unit'),
            'status' => request('status')
        ];

        $Commodity = Commodity::create($data);

        if($Commodity instanceof Commodity){
            $Commodity->categories()->sync(request('categories'));

            return redirect()->route('admin.commodities.all');
        }
        return redirect()->route('admin.commodities.all');
    }


    public function edit(Commodity $commodity)
    {
        $statuses = Constant::getStatusesViewer();
        $selectedDataIDs = $commodity->categories()->pluck('id')->toArray();
        $categories = JsTree::getDataFor(Category::class, 'parent_id', null, $selectedDataIDs);

        return view('admin.commodities.edit', compact('commodity', 'statuses', 'categories'));
    }


    public function update(Commodity $commodity)
    {
        //validation
        request()->validate([
            'title' => ['required'],
            'stock' => ['required'],
            'unit' => ['required'],
            'description' => ['nullable', 'string'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
            'categories' => ['required']
        ], [
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
        ]);

        // get data
        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'stock' => request('stock'),
            'unit' => request('unit'),
            'status' => request('status')
        ];

        $commodity->update($data);
        $commodity->categories()->sync(request('categories'));

        return redirect()->route('admin.commodities.all');
    }


    public function delete(Commodity $commodity)
    {
//        Commodity::destroy($commodity->id);
        $commodity->delete();

        return redirect()->back();
    }
}
