<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdministratorsController extends BaseController
{
    public function index()
    {
        $administrators = Administrator::latest()->paginate();

        return view('admin.administrators.all', compact('administrators'));
    }


    public function create()
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.administrators.create', compact('statuses'));
    }


    public function store(Request $request)
    {
        // validationStoreForm
        $request->validate([
            'first_name' =>['required'],
            'last_name' =>['required'],
            'mobile' =>['required', 'unique:administrators,mobile'],
            'email' =>['nullable', 'email', 'unique:administrators,email'],
            'username' =>['nullable', 'unique:administrators,username'],
            'password' =>['required', 'min:8', 'confirmed'],
            'avatar' => ['nullable', 'mimes:jpeg,jpg,png,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
        ], [
            '*.required' => 'پر کردن فیلد مربوطه الزامی است.',
            'mobile.unique' => 'تلفن وارد شده قبلا ثبت شده.',
            'email.unique' => 'ایمیل وارد شده قبلا ثبت شده.',
            'username.unique' => 'نام کاربری وارد شده قبلا ثبت شده.',
            'avatar.mimes' => 'فرمت عکس مورد نظر اشتباه میباشد.',
            'password.min' => 'رمز وارد شده حداقل باید 8 کارکتر داشته باشد.',
            'password.confirmed' => 'تایید رمز عبور مطابقت ندارد.'
        ]);

        // getData and create admin in database
        $data =[
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'mobile' => $request->input('mobile'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'status' => $request->input('status'),
        ];
        if ( $request->file('avatar')) {
            $data['avatar'] = $this->uploadImage($request->file('avatar'), Constant::ADMIN_IMAGE_PATH);
        }
        $administrator = Administrator::create($data);

        // return
        if ($administrator instanceof Administrator) {
            return redirect()->route('admin.administrators.all')->with('success', 'مدیر مورد نظر با موفقیت ثبت شد.');
        }
        return  redirect()->back();
    }


    public function edit(Administrator $administrator)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.administrators.edit', compact('administrator', 'statuses'));
    }


    public function update(Request $request, Administrator $administrator)
    {
        // validation
        $request->validate([
            'first_name' =>['required'],
            'last_name' =>['required'],
            'mobile' =>['required', Rule::unique('administrators')->ignore($administrator)],
            'email' =>['nullable', 'email', Rule::unique('administrators')->ignore($administrator)],
            'username' =>['nullable', Rule::unique('administrators')->ignore($administrator)],
            'password' =>['required', 'min:8', 'confirmed'],
            'avatar' => ['nullable', 'mimes:jpeg,jpg,png,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
        ], [
            '*.required' => 'پر کردن فیلد مربوطه الزامی است.',
            'mobile.unique' => 'تلفن وارد شده قبلا ثبت شده.',
            'email.unique' => 'ایمیل وارد شده قبلا ثبت شده.',
            'username.unique' => 'نام کاربری وارد شده قبلا ثبت شده.',
            'avatar.mimes' => 'فرمت عکس مورد نظر اشتباه میباشد.',
            'password.min' => 'رمز وارد شده حداقل باید 8 کارکتر داشته باشد.',
            'password.confirmed' => 'تایید رمز عبور مطابقت ندارد.'
        ]);

        // update data
        $data =[
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'mobile' => $request->input('mobile'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'status' => $request->input('status'),
        ];
        if ( $request->file('avatar')) {
            $data['avatar'] = $this->uploadImage($request->file('avatar'), Constant::ADMIN_IMAGE_PATH);
        }

        if ($request->input('password')) {
            $data['password'] = Hash::make($request->input('password'));
        }

        $administrator->update($data);

        return redirect()->route('admin.administrators.all');
    }


    public function delete(Administrator $administrator)
    {
        $administrator->delete();

        return redirect()->back();
    }
}
