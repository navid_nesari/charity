<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Commodity;
use App\Models\Help;
use App\Models\HelpDetails;
use App\Models\User;
use App\Models\Needy;
use Illuminate\Http\Request;

class HelpDetailsController extends Controller
{
    
    public function detailCommodity(Needy $needy, Commodity $commodity)
    {
        return response()->json($commodity);
    }



    public function createDetail(string $needy, $help, $user, $entity)
    {
        $userObject = User::where('id', $user)->first();
        $helpObject = Help::where('id', $help)->first();
        $commodities = Commodity::select('id', 'title', 'stock')->get()->toArray();
        $help_details = HelpDetails::latest()->paginate();

        return view('admin.helps.create-detail', compact(
            'needy',
            'helpObject',
            'userObject',
            'entity',
            'commodities',
            'help_details'
        ));
    }



    public function storeDetail(string $needy, $help, $user, $entity)
    {
        //validation
        request()->validate([
            'unit' => ['required', 'numeric'],
            'commodity' => ['required'],
        ],[
            '*.required' => 'فیلد مربوطه الزامی است.',
            'unit.numeric' => 'فیلد مربوطه عددی است.'
        ]);

        //getData
        $data = [
            'entity_id' => request('commodity'),
            'entity_type' => request('entity'),
            'detail' => request('unit'),
        ];

        $help_detail = HelpDetails::create($data);

        if($help_detail instanceof HelpDetails){
            return redirect()->route('admin.helps.create.detail',['needy'=>$needy, 'help'=>$help, 'user'=>$user, 'entity'=>$entity]);
        }

        return redirect()->back();
    }



    public function updateDetail(string $needy, $help, $user, $entity, HelpDetails $help_detail)
    {
        //validation
        request()->validate([
            'unit' => ['required', 'numeric'],
            'commodity' => ['required'],
        ],[
            '*.required' => 'فیلد مربوطه الزامی است.',
            'unit.numeric' => 'فیلد مربوطه عددی است.'
        ]);

        //getData
        $data = [
            'entity_id' => request('commodity'),
            'entity_type' => request('entity'),
            'detail' => request('unit'),
        ];
        $help_detail->update($data);

        return redirect()->route('admin.helps.create.detail',['needy'=>$needy, 'help'=>$help, 'user'=>$user, 'entity'=>$entity]);
    }


    public function deleteDetail(string $needy, $help, $user, $entity, HelpDetails $help_detail)
    {
        $help_detail->delete();

        return redirect()->route('admin.helps.create.detail',['needy'=>$needy, 'help'=>$help, 'user'=>$user, 'entity'=>$entity]);
    }
}
