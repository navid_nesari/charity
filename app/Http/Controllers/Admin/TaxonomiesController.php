<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\Controller;
use App\Models\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class TaxonomiesController extends Controller
{
    public function __construct()
    {
        if(!in_array(request('entity'), array_keys(Constant::getTaxonomiesEntities()))){
            abort(404);
        }
    }


    public function index(string $entity)
    {
        $statuses =Constant::getStatusesViewer();
        $taxonomies = Taxonomy::where('entity', $entity)->latest()->paginate();

        return view('admin.taxonomies.all', compact('taxonomies', 'entity', 'statuses'));
    }


    public function store(string $entity)
    {
        request()->validate([
            'title' => ['required'],
            'description' => ['nullable', 'string'],
            'entity_hidden' => ['required', Rule::in(array_keys(Constant::getTaxonomiesEntities()))],
        ],
         [
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
            'entity.required' => '',
            'entity.in' => 'نوع کمک معتبر نیست!',
        ]);

        if(! $this->isValidEntity()){
            return redirect()->route('admin.taxonomies.all', ['entity'=> request('entity')]);
        }

        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'entity' => request('entity'),
        ];

        $taxonomy = Taxonomy::create($data);

        if($taxonomy instanceof Taxonomy){
//            $taxonomy->investigators()->attach(\request());
            return redirect()->route('admin.taxonomies.all', ['entity'=> request('entity')]);
        }
        return redirect()->route('admin.taxonomies.all', ['entity'=> request('entity')]);

    }


    public function edit(string $entity, Taxonomy $taxonomy)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.taxonomies.edit', compact('entity', 'taxonomy', 'statuses'));
    }


    public function update(string $entity, Taxonomy $taxonomy)
    {
        //validation
        request()->validate([
            'title' => ['required'],
            'description' => ['nullable', 'string'],
            'entity_hidden' => ['required', Rule::in(array_keys(Constant::getTaxonomiesEntities()))],
        ], [
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
            'entity.required' => '',
            'entity.in' => 'نوع کمک معتبر نیست!',
        ]);

        if(! $this->isValidEntity()){
            return redirect()->route('admin.taxonomies.all', ['entity'=> request('entity')]);
        }

        // get data
        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'entity' => request('entity'),
        ];

        $taxonomy->update($data);

        return redirect()->route('admin.taxonomies.all', ['entity' => $entity]);
    }


    public function delete(string $entity, Taxonomy $taxonomy)
    {
        $taxonomy->delete();

        return redirect()->route('admin.taxonomies.all', ['entity'=> request('entity')]);
    }


    public function isValidEntity()
    {
        // request('entity') = entity in url page -> for example = plan
        if( !is_null(request('entity_hidden')) ||  !is_null(request('entity'))){
            if(request('entity_hidden') === request('entity')){
                if(in_array(request('entity_hidden'), array_keys(Constant::getTaxonomiesEntities())) &&
                    in_array(request('entity'), array_keys(Constant::getTaxonomiesEntities()) )){
                    return true;
                }
            }
        }

    }
}
