<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\Controller;
use App\Models\Investigator;
use App\Models\Needy;
use App\Models\Taxonomy;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class InvestigatorsController extends Controller
{

    public function index()
    {
       $investigators = Investigator::latest()->paginate();

       return view('admin.investigators.all', compact('investigators'));
    }



    public function create()
    {
        $statuses = Constant::getInvestigatorStatusesViewer();
        $neediesTypes = Constant::getNeediesTypesViewer();
        $plansHelps = Taxonomy::where('entity', Constant::PLAN)->select('id', 'title')->get()->toArray();
        $housesTypes = Constant::getHousesTypesViewer();
        $identificationDocsTypes = Constant::getIdentificationDocsTypesViewer();

        return view('admin.investigators.create', compact(
            'statuses',
            'neediesTypes',
            'plansHelps',
            'identificationDocsTypes',
            'housesTypes'
        ));
    }



    public function store(Request $request)
    {
        // validation
        $this->validateStoreForm($request);

        // getStoreData
        $data = $this->getData($request);

        // create new investigator
        $investigator = Investigator::create($data);

        // user create
        if ($investigator instanceof Investigator) {
            if ($request->input('status') == Constant::APPROVED) {
                // user create
                $userData = [
                    'investigator_id' => $investigator->id,
                    'first_name' => \request('first_name'),
                    'last_name' => \request('last_name'),
                    'code_melli' => \request('code_melli'),
                    'address' => \request('address'),
                ];
                $user = User::create($userData);

                //needy create
                $needyData = [
                    'user_id' => $user->id,
                    'approved_date' =>'2023-5-6'
                ];
                $needy = Needy::create($needyData);
            }

            $investigator->taxonomies()->attach( request('plans') );

            return redirect()->route('admin.investigators.all')->with('success', 'فرد مورد نظر با موفقیت ثبت شد.');
        }

        return  redirect()-> back();
    }



    public function edit(Investigator $investigator)
    {
        $statuses = Constant::getInvestigatorStatusesViewer();
        $neediesTypes = Constant::getNeediesTypesViewer();
        // I think $planHelps = getTaxonomiesEntitiesViewer() bat bellow???
        $plansHelps = Taxonomy::where('entity', Constant::PLAN)->select('id', 'title')->get()->toArray();
        $housesTypes = Constant::getHousesTypesViewer();
        $identificationDocsTypes = Constant::getIdentificationDocsTypesViewer();

        return view('admin.investigators.edit', compact(
            'investigator',
            'statuses',
            'neediesTypes',
            'plansHelps',
            'identificationDocsTypes',
            'housesTypes'
        ));
    }



    public function update(Request $request, Investigator $investigator)
    {
        // validation
        $this->validateUpdateForm($request, $investigator);

        // getData
        $data = $this->getData($request);

        // update investigator
        $investigator->update($data);

        if ($request->input('status') == Constant::APPROVED && !isset($investigator->user->id)){

            // user update
            $userData = [
                'investigator_id' => $investigator->id,
                'first_name' => \request('first_name'),
                'last_name' => \request('last_name'),
                'code_melli' => \request('code_melli'),
                'address' => \request('address'),
            ];
            $user = User::create($userData);

            // needy update
            $user_id = $investigator->user->id;
            $needyData = [
                'user_id' => $user_id,
                'approved_date' =>'2023-5-6'
            ];
            $needy = Needy::create($needyData);

            // return redirect
            return redirect()->route('admin.investigators.all')->with('success', 'تغییرات مورد نظر با موفقیت ثبت شد.');
        }

        if ($request->input('status') == Constant::REJECTED) {
            $user = $investigator->user;
            $needy = $user->needy;
            $user->delete();
            $needy->delete();

            return redirect()->route('admin.investigators.all')->with('success', 'تغییرات مورد نظر با موفقیت ثبت شد.');
        }

        return redirect()->route('admin.investigators.all')->with('success', 'تغییرات مورد نظر با موفقیت ثبت شد.');
    }



    public function delete(Investigator $investigator)
    {
        $investigator->delete();

        return redirect()->back();
    }



    private function validateStoreForm(Request $request)
    {
        $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'code_melli' => ['required', 'unique:investigators,code_melli'],
            'house_hold_number' => ['required'],
            'introducer' => ['required'],
            'investigate_date' => ['required'],
            'address' => ['required'],
            'result' => ['required'],
            'status' => ['required'],
            'needies_types' => ['required'],
            'plans' => ['required'],
            'houses_types' => ['required'],
            'identification_docs_types' => ['required'],
            'parent' => ['nullable']
        ],[
            '*.required' => 'پر کردن فیلد مربوطه الزامی است.',
            'code_melli.unique' => 'کد ملی وارد شده قبلا ثبت شده.',
        ]);
    }



    private function validateUpdateForm(Request $request, Investigator $investigator)
    {
        $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'code_melli' => ['required', Rule::unique('investigators')->ignore($investigator)],
            'house_hold_number' => ['required'],
            'introducer' => ['required'],
            'investigate_date' => ['required'],
            'address' => ['required'],
            'result' => ['required'],
            'status' => ['required', Rule::in(Constant::APPROVED, Constant::REJECTED)],
            'needies_types' => ['required'],
            'plans' => ['required'],
            'houses_types' => ['required'],
            'identification_docs_types' => ['required'],
            'parent' => ['nullable']
        ],[
            '*.required' => 'پر کردن فیلد مربوطه الزامی است.',
            'code_melli.unique' => 'کد ملی وارد شده قبلا ثبت شده.',
        ]);
    }



    private function getData(Request $request): array
    {
       $data = [
           'first_name' => $request->input('first_name'),
           'last_name' => $request->input('last_name'),
           'code_melli' => $request->input('code_melli'),
           'house_hold_number' => $request->input('house_hold_number'),
           'introducer' => $request->input('introducer'),
           'investigate_date' => Date::toCarbonDateFormat($request->input('investigate_date')),
           'address' => $request->input('address'),
           'result' => $request->input('result'),
           'status' => $request->input('status'),
           'needies_types' => $request->input('needies_types'),
           'houses_types' => $request->input('houses_types'),
           'identification_docs_types' => $request->input('identification_docs_types'),
       ];

       return $data;
    }
}
