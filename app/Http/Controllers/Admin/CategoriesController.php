<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->paginate();

        return view('admin.categories.all', compact('categories'));
    }


    public function store()
    {
//        dd(\request()->all());
        request()->validate([
            'title' => ['required'],
            'description' => ['nullable', 'string'],
        ],[
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
        ]);

        $data = [
            'title' => request('title'),
            'description' => request('description'),
        ];

        $category = Category::create($data);

        if($category instanceof Category){
            return redirect()->route('admin.categories.all');
        }
        return redirect()->route('admin.categories.all');
    }


    public function edit(Category $category)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.categories.edit', compact('category', 'statuses'));
    }


    public function update(Category $category)
    {
        //validation
        request()->validate([
            'title' => ['required'],
            'description' => ['nullable', 'string'],
        ], [
            'title.required' => 'فیلد مورد نظر الزامی است.',
            'description.string' => '',
        ]);

        // get data
        $data = [
            'title' => request('title'),
            'description' => request('description'),
        ];

        $category->update($data);

        return redirect()->route('admin.categories.all');
    }


    public function delete(Category $category)
    {
        $category->delete();

        return redirect()->back();
    }
}
