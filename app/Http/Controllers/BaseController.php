<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class BaseController extends Controller
{
    public function uploadImage(UploadedFile $file, $path)
    {
        $imageName = time().'.'.$file->extension();

        // Public Folder
        $file->move(public_path($path), $imageName);

        return $imageName;
    }
}
