<?php

namespace Database\Seeders;

use App\Constants\Constant;
use App\Models\Administrator;
use App\Models\Investigator;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Administrator::Create([
//            'first_name' => 'navid',
//            'last_name' => 'nesari',
//            'mobile' => '09363369392',
//            'email' => 'navid@gmail.com',
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//        ]);
        User::Create([
            'first_name' => 'navid',
            'last_name' => 'nesari',
            'mobile' => '09363369392',
            'email' => 'navid@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);
        Investigator::Create([
            'user_id' => '1',
            'first_name' => 'navid',
            'last_name' => 'nesari',
            'code_melli' => '',
            'house_hold_number' => '5',
            'introducer' => 'GOD',
            'investigate_date' => '1402/04/29',
            'address' => 'fgd dfgdf dfg drgd ',
            'result' => 'bhcdx dgh nfsrtyfj fdgtuyh fghj fj.',
            'status' => Constant::APPROVED,
            'needies_types' => '',
            'houses_types' => '',
            'identification_docs_types' => '',
        ]);


//        Administrator::factory(10)->create();
//         \App\Models\User::factory(10)->create();
    }
}
