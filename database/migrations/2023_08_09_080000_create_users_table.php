<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->unsignedBigInteger('city_id');
//            $table->foreign('city_id')->references('id')->on('cities')->cascadeOnDelete();
//2014_10_12_000000_create_users_table
            $table->unsignedBigInteger('investigator_id');
            $table->foreign('investigator_id')->references('id')->on('investigators')->cascadeOnDelete();

            $table->string('first_name');
            $table->string('last_name');
            $table->date('birth_date')->nullable();
            $table->string('code_melli')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->text('address');
            $table->string('avatar')->nullable();
            $table->enum('status', [Constant::ACTIVE, Constant::IN_ACTIVE])->default(Constant::ACTIVE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
