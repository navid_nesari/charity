<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxonomiablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomiables', function (Blueprint $table) {
            $table->unsignedBigInteger('taxonomy_id');
            $table->foreign('taxonomy_id')->references('id')->on('taxonomies')->cascadeOnDelete();

            $table->unsignedBigInteger('entity_id')->index();
            $table->string('entity_type')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxonomiables');
    }
}
