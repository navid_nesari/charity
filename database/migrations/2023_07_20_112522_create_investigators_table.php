<?php

use App\Constants\Constant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestigatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('code_melli')->unique();
            $table->string('house_hold_number');
            $table->string('introducer');
            $table->date('investigate_date');
            $table->text('address');
            $table->text('result');
            $table->string('needies_types');
            $table->string('houses_types');
            $table->string('identification_docs_types');
            $table->enum('status', [Constant::APPROVED, Constant::REJECTED])->default(Constant::APPROVED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigators');
    }
}
