<?php

use App\Http\Controllers\Admin\AdministratorsController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\CommoditiesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\HelpDetailsController;
use App\Http\Controllers\Admin\HelpsController;
use App\Http\Controllers\Admin\InvestigatorsController;
use App\Http\Controllers\Admin\NeediesController;
use App\Http\Controllers\Admin\TaxonomiesController;
use App\Http\Controllers\Admin\UsersController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => ''], function () {

    //dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    // administrators
    Route::group(['prefix' => 'administrators'], function () {
        Route::get('/all', [AdministratorsController::class, 'index'])
            ->name('admin.administrators.all');

        Route::get('/create', [AdministratorsController::class, 'create'])
            ->name('admin.administrators.create');

        Route::post('/store', [AdministratorsController::class, 'store'])
            ->name('admin.administrators.store');

        Route::get('/edit/{administrator}', [AdministratorsController::class, 'edit'])
            ->name('admin.administrators.edit');

        Route::post('/update/{administrator}', [AdministratorsController::class, 'update'])
            ->name('admin.administrators.update');

        Route::get('/delete/{administrator}', [AdministratorsController::class, 'delete'])
            ->name('admin.administrators.delete');
    });


    // investigators
    Route::group(['prefix' => 'investigators'], function () {
        Route::get('/all', [InvestigatorsController::class, 'index'])
            ->name('admin.investigators.all');

        Route::get('/create', [InvestigatorsController::class, 'create'])
            ->name('admin.investigators.create');

        Route::post('/store', [InvestigatorsController::class, 'store'])
            ->name('admin.investigators.store');

        Route::get('/edit/{investigator}', [InvestigatorsController::class, 'edit'])
            ->name('admin.investigators.edit');

        Route::post('/update/{investigator}', [InvestigatorsController::class, 'update'])
            ->name('admin.investigators.update');

        Route::get('/delete/{investigator}', [InvestigatorsController::class, 'delete'])
            ->name('admin.investigators.delete');
    });


    // taxonomies
    Route::group(['prefix' => 'taxonomies/{entity}'], function () {
        Route::get('/all', [TaxonomiesController::class, 'index'])
            ->name('admin.taxonomies.all');

        Route::post('/store', [TaxonomiesController::class, 'store'])
            ->name('admin.taxonomies.store');

        Route::get('/edit/{taxonomy}', [TaxonomiesController::class, 'edit'])
            ->name('admin.taxonomies.edit');

        Route::post('/update/{taxonomy}', [TaxonomiesController::class, 'update'])
            ->name('admin.taxonomies.update');

        Route::get('/delete/{taxonomy}', [TaxonomiesController::class, 'delete'])
            ->name('admin.taxonomies.delete');
    });


    // users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/all', [UsersController::class, 'index'])
            ->name('admin.users.all');

        Route::get('/delete/{user}', [UsersController::class, 'delete'])
            ->name('admin.users.delete');
    });


    // needies
    Route::group(['prefix' => 'needies'], function () {
        Route::get('/all-families-head', [NeediesController::class, 'familiesHeadAll'])
            ->name('admin.needies.families.head.all');

        Route::get('/create', [NeediesController::class, 'create'])
            ->name('admin.needies.create');

        Route::get('/delete/{needy}', [NeediesController::class, 'delete'])
            ->name('admin.needies.delete');


        Route::group(['prefix' => 'families-head/{needy}'], function () {
            Route::get('/create', [NeediesController::class, 'familiesMemberCreate'])
                 ->name('admin.needies.families.member.create');

            Route::post('/store', [NeediesController::class, 'familiesMemberStore'])
                 ->name('admin.needies.families.member.store');

            Route::get('/edit', [NeediesController::class, 'familiesMemberEdit'])
                 ->name('admin.needies.families.member.edit');

            Route::post('/update', [NeediesController::class, 'familiesMemberUpdate'])
                 ->name('admin.needies.families.member.update');

            Route::get('/delete', [NeediesController::class, 'familiesMemberDelete'])
                 ->name('admin.needies.families.member.delete');
        });
    });


    // categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/all', [CategoriesController::class, 'index'])
            ->name('admin.categories.all');

        Route::post('/store', [CategoriesController::class, 'store'])
            ->name('admin.categories.store');

        Route::get('/edit/{category}', [CategoriesController::class, 'edit'])
            ->name('admin.categories.edit');

        Route::post('/update/{category}', [CategoriesController::class, 'update'])
            ->name('admin.categories.update');

        Route::get('/delete/{category}', [CategoriesController::class, 'delete'])
            ->name('admin.categories.delete');
    });


    // commodities
    Route::group(['prefix' => 'commodities'], function () {
        Route::get('/all', [CommoditiesController::class, 'index'])
            ->name('admin.commodities.all');

        Route::get('/create', [CommoditiesController::class, 'create'])
            ->name('admin.commodities.create');

        Route::post('/store', [CommoditiesController::class, 'store'])
            ->name('admin.commodities.store');

        Route::get('/edit/{commodity}', [CommoditiesController::class, 'edit'])
            ->name('admin.commodities.edit');

        Route::post('/update/{commodity}', [CommoditiesController::class, 'update'])
            ->name('admin.commodities.update');

        Route::get('/delete/{commodity}', [CommoditiesController::class, 'delete'])
            ->name('admin.commodities.delete');
    });

    // helps
    Route::group(['prefix' => 'helps/{needy}'], function () {
        Route::get('/all', [HelpsController::class, 'all'])
            ->name('admin.helps.all');

        Route::post('/store', [HelpsController::class, 'store'])
            ->name('admin.helps.store');

        Route::get('/edit/{help}', [HelpsController::class, 'edit'])
            ->name('admin.helps.edit');

        Route::post('/update/{help}', [HelpsController::class, 'update'])
            ->name('admin.helps.update');

        Route::get('/delete/{help}', [HelpsController::class, 'delete'])
            ->name('admin.helps.delete');



        // help_details
        Route::get('/create-detail/{help}/{user}/{entity}', [HelpDetailsController::class, 'createDetail'])
            ->name('admin.helps.create.detail');

        Route::get('/create-detail-commodities/{commodity}', [HelpDetailsController::class, 'detailCommodity'])
            ->name('admin.helps.create.detail.commodity');

        Route::post('/store-detail/{help}/{user}/{entity}', [HelpDetailsController::class, 'storeDetail'])
            ->name('admin.helps.store.detail');

        Route::post('/edit-detail/{help}/{user}/{entity}/{help_detail}', [HelpDetailsController::class, 'updateDetail'])
            ->name('admin.helps.update.detail');

        Route::get('/delete-detail/{help}/{user}/{entity}/{help_detail}', [HelpDetailsController::class, 'deleteDetail'])
            ->name('admin.helps.delete.detail');
    });

});
