@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"
          integrity="sha512-gOQQLjHRpD3/SEOtalVq50iDn4opLVup2TF8c4QPI3/NmUPNZOk2FG0ihi8oCU/qYEsw4P6nuEZT2lAG0UNYaw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
@endsection

@section('content')
    <div class="card ms-3 me-3">
        <div class="app-container container-xxl d-flex flex-stack py-3">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-4">
                    فهرست افراد
                </h1>
                <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                 تعداد کل :
                    <i class="badge badge-sm badge-light-warning px-2"
                       style="font-size: 10px">{{ count($investigators) }}</i>
                </span>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">
                <a href="{{ route('admin.investigators.create') }}" class="btn btn-sm fw-bold btn-primary">
                    ثبت جدید
                </a>
            </div>
        </div>
        <div class="card-body py-4">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                    <!--begin::Table head-->
                    <thead>
                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                        <th class="min-w-125px sorting">نام</th>
                        <th class="min-w-125px sorting">نام خانوادگی</th>
                        <th class="min-w-125px sorting">کد ملی</th>
                        <th class="min-w-125px sorting">وضعیت</th>
                        <th class="min-w-125px sorting">تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                    @foreach($investigators as $investigator)
                        <tr>
                            <td>{{ $investigator->first_name }}</td>
                            <td>{{ $investigator->last_name }}</td>
                            <td>{{ $investigator->code_melli }}</td>
                            <td>{!! $investigator->webPresent()->status !!}</td>
                            <td>
                                <a href="{{ route('admin.investigators.edit', $investigator) }}"
                                   class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                   data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                    <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-edit"></i>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>


                                <a href="javascript:;"
                                   data-investigator-id="{{$investigator->id}}"
                                   class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_investigator"
                                   data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                    <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>
                            </td>
                            <!--end::Action=-->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>

    </div>
@endsection

@section('script')

    <script src="https://code.jquery.com/jquery-3.7.0.min.js"
            integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.20/sweetalert2.min.js"
            integrity="sha512-2AOp8GEFv1X43dZpYqOp34WD+skmM18yOrCxS/S1Mh6VShz7uxlPhKmA98fsPrE7MMMtZgjshiMHKmzWtZR5uA=="
            crossorigin="anonymous" referrerpolicy="no-referrer">

    </script>

    <script type="text/javascript">
        $('.delete_investigator').click(function (e) {
            e.preventDefault();

            // console.log('delete blog click !');
            console.log('investigator id : ' + $(this).data('investigator-id'));

            let investigator_id = $(this).data('investigator-id');

            let url = "{{ route('admin.investigators.delete',':investigator-id') }}";
            // console.log(url);
            url = url.replace(':investigator-id', investigator_id)
            // console.log(url);

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

        // set session to message(sweetalert) is shown `only once` -> flash session
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>

@endsection


