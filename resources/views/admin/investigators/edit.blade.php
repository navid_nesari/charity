@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min.css')}}"/>
@endsection

@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.investigators.update', $investigator) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
                <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش فرد</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.investigators.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => ' نام', 'required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                            'name' => 'first_name',
                                                            'placeholder' => 'نام و نام خانوادگی',
                                                            'value' => $investigator->first_name
                                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'نام خانوادگی', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                                        'name' => 'last_name',
                                                        'placeholder' => 'نام خانوادگی',
                                                        'value' => $investigator->last_name
                                                        ])
                                    <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'کد ملی', 'required' => 1])
                                        <!--end::Label-->

                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                            'name' => 'code_melli',
                                                            'placeholder' => 'کد ملی',
                                                            'value' => $investigator->code_melli
                                                            ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'تعداد خانوار', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                                        'name' => 'house_hold_number',
                                                        'placeholder' => 'تعداد خانوار',
                                                        'value' => $investigator->house_hold_number
                                                        ])
                                    <!--end::Input-->

                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'معرف'])
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                                        'name' => 'introducer',
                                                        'placeholder' => 'معرف',
                                                        'value' => $investigator->introducer
                                                        ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-8 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label',['title' => 'تاریخ تحقیق'])
                                        <!--end::Label-->
                                        <!--begin::Input-->
{{--                                        @dd($investigator->investigate_date)--}}
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="investigate_date" id="investigate_date"
                                                   placeholder="تاریخ تحقیق" value="{{old('investigate_date', $investigator->investigate_date)}}">
                                        </div>
                                        @error('investigate_date')
                                        <p class="text-danger">{{$message}}</p>
                                        @enderror
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-8 fv-row fv-plugins-icon-container">
                                        @include('admin.__components.label', ['title' => 'نوع خانواده'])
                                        @include('admin.__components.horizontal-checkbox',[
                                                 'name' => 'needies_types',
                                                 'items' => $neediesTypes,
                                                 'activeKey' => $investigator->needies_types
                                                ])
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
{{--                            @dd($investigator->taxonomies()->pluck('id')->toArray())--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-8 fv-row fv-plugins-icon-container">
                                        @include('admin.__components.label', ['title' => 'نوع کمک'])
                                        @include('admin.__components.horizontal-checkbox',[
                                                 'name' => 'plans',
                                                 'items' => $plansHelps,
                                                 'activeKey' => $investigator->taxonomies()->get()->pluck('id')->toArray()
                                                ])
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-8 fv-row fv-plugins-icon-container">
                                        @include('admin.__components.label', ['title' => 'نوع خانه'])
                                        @include('admin.__components.horizontal-checkbox',[
                                                 'name' => 'houses_types',
                                                 'items' => $housesTypes,
                                                 'activeKey' => $investigator->houses_types
                                                ])
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        @include('admin.__components.label', ['title' => 'نوع مدرک'])
                                        @include('admin.__components.horizontal-checkbox',[
                                                 'name' => 'identification_docs_types',
                                                 'items' => $identificationDocsTypes,
                                                 'activeKey' => $investigator->identification_docs_types
                                                ])
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                         @include('admin.__components.label',['title' => 'آدرس'])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.textarea', [
                                                'name' => 'address',
                                                'placeholder' => 'آدرس',
                                                'value' => $investigator->address
                                                ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'نتیجه'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.textarea', [
                                            'name' => 'result',
                                            'placeholder' => 'نتیجه',
                                            'value' => $investigator->result
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <div class="d-flex justify-content-start ms-1">
                        <!--begin::Button-->
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت تغییرات</span>
                        </button>
                        <!--end::Button-->
                    </div>
                </div>
                <!--end::Main column-->
                <!--begin::Aside column-->
                <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-3 me-lg-2">
                    <!--begin::Status-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>وضعیت</h4>
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar">
                                <div class="rounded-circle bg-success w-15px h-15px"
                                     id="kt_ecommerce_add_category_status"></div>
                            </div>
                            <!--begin::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            <div class="fv-row fv-plugins-icon-container">
                                @include('admin.__components.horizontal-radiobutton',[
                                        'items' => $statuses,
                                        'name' => 'status',
                                        'activeKey' => $investigator->status,
                                ])
                            </div>

                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->

                </div>
                <!--end::Aside column-->

            </form>
        </div>
    </div>
@endsection
@section('script')

    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>

    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $("#investigate_date").pDatepicker({
                initialValue : true,
                format: 'L'
            });

        });

    </script>

@endsection
