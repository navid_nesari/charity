@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min.css')}}"/>
@endsection

@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                   action="{{ route('admin.investigators.store') }}" method="post"
                  enctype="multipart/form-data">
            @csrf
                <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>افزودن فرد</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{ route('admin.investigators.all') }}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <div class="row mb-10">
                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => ' نام', 'required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'first_name',
                                                'placeholder' => 'نام',
                                                ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'نام خانوادگی', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'last_name',
                                            'placeholder' => 'نام خانوادگی'
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'کد ملی', 'required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'code_melli',
                                                'placeholder' => 'کد ملی',
                                                ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row mb-10">
                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'تعداد خانوار', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'house_hold_number',
                                            'placeholder' => 'تعداد خانوار'
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'معرف','required' => 1])
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'introducer',
                                            'placeholder' => 'معرف',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label',['title' => 'تاریخ تحقیق','required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="investigate_date" id="investigate_date"
                                            placeholder="تاریخ تحقیق" value="{{old('investigate_date')}}">
                                        </div>
                                        @error('investigate_date')
                                        <p class="text-danger">{{$message}}</p>
                                        @enderror
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row mb-10">
                               <div class="col-12">
                                   <div class="form-group">
                                       @include('admin.__components.label', ['title' => 'نوع خانواده'])
                                       @include('admin.__components.horizontal-checkbox', [
                                            'name' => 'needies_types',
                                            'items' => $neediesTypes
                                        ])
                                   </div>
                               </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row mb-10">
                               <div class="col-12">
                                   <div class="form-group">
                                       @include('admin.__components.label', ['title' => 'نوع کمک'])
                                       @include('admin.__components.horizontal-checkbox', [
                                            'name' => 'plans',
                                            'items' => $plansHelps
                                        ])
                                   </div>
                               </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row mb-10">
                                <div class="col-12">
                                    <div class="form-group">
                                        @include('admin.__components.label', ['title' => 'نوع خانه'])
                                        @include('admin.__components.horizontal-checkbox', [
                                             'name' => 'houses_types',
                                             'items' => $housesTypes
                                         ])
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row mb-10">
                                <div class="col-12">
                                    <div class="form-group">
                                        @include('admin.__components.label', ['title' => 'نوع مدرک'])
                                        @include('admin.__components.horizontal-checkbox', [
                                             'name' => 'identification_docs_types',
                                             'items' => $identificationDocsTypes
                                         ])
                                    </div>
                                </div>
                            </div>
                            @include('admin.__components.separator-dashed')
                            <div class="row mb-10">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'آدرس'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.textarea', [
                                            'name' => 'address',
                                            'placeholder' => 'آدرس',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-10">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'نتیجه'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.textarea', [
                                            'name' => 'result',
                                            'placeholder' => 'نتیجه',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <div class="d-flex justify-content-start ms-1">
                        <!--begin::Button-->
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت فرد</span>

                        </button>
                        <!--end::Button-->
                    </div>
                </div>
                <!--end::Main column-->
                <!--begin::Aside column-->
                <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-3 me-lg-2">
                    <!--begin::Status-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>وضعیت</h4>
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar">
                                <div class="rounded-circle bg-success w-15px h-15px"
                                     id="kt_ecommerce_add_category_status"></div>
                            </div>
                            <!--begin::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            <div class="fv-row fv-plugins-icon-container">

                                @include('admin.__components.horizontal-radiobutton',[
                                        'items' => $statuses,
                                        'name' => 'status',
                                        'activeKey' => \App\Constants\Constant::APPROVED,
                                ])

                            </div>

                            <!--end::Datepicker-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>--}}

    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>

    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $("#investigate_date").pDatepicker({
                initialValue : true,
                format: 'L'
            });

        });

    </script>
@endsection
