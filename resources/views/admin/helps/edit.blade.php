@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min.css')}}"/>
@endsection

@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.helps.update', ['needy' => $needy, $help]) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
            <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>ویرایش کمک</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.helps.all', ['needy' => $needy])}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'عنوان', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                                        'name' => 'title',
                                                        'placeholder' => 'عنوان',
                                                        'value' => $help->title
                                                        ])
                                    <!--end::Input-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'needy_hidden',
                                            'type' => 'hidden',
                                            'value' => $needy
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        @include('admin.__components.label', ['title' => 'تاریخ کمک'])
                                        @include('admin.__components.datepicker', [
                                                 'name' => 'done_at',
                                                 'value' => $help->done_at
                                                 ])
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'توضیحات'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.textarea', [
                                            'name' => 'description',
                                            'placeholder' => 'توضیحات',
                                            'value' => $help->description
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <div class="d-flex justify-content-start ms-1">
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت تغییرات</span>
                        </button>
                        <!--end::Button-->
                    </div>
                </div>
                <!--end::Main column-->

            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>

    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>
@endsection
