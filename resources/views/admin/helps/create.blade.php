<form action="{{ route('admin.helps.store', $needy) }}" method="POST">
    @csrf
    <div class="card ms-3">
        <div class="card-header">
            <h3 class="card-title">ثبت کمک جدید</h3>
        </div>
        <div class="card-body">
            <div class="form-group mb-8">
                @include('admin.__components.label', ['title' => 'عنوان', 'required' => 1])
                @include('admin.__components.input-text', [
                         'name' => 'title',
                         'placeholder' => 'عنوان'
                         ])
                @include('admin.__components.input-text', [
                         'name' => 'needy_hidden',
                         'type' => 'hidden',
                         'value' => $needy
                        ])
            </div>
            <div class="form-group mb-8">
                @include('admin.__components.label', ['title' => 'تاریخ کمک'])
                @include('admin.__components.datepicker', [
                         'name' => 'done_at'
                         ])
            </div>
            <div class="form-group">
                @include('admin.__components.label', ['title' => 'توضیحات'])
                @include('admin.__components.textarea', ['name' => 'description'])
            </div>
            @include('admin.__components.separator-dashed')
            <div class="form-group">
                <button class="btn btn-success form-control">ثبت</button>
            </div>
        </div>
    </div>
</form>