@extends('layouts.master')

@section('style')
@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'جزئیات کمک'])
@endsection

@section('content')
    <div class="row mb-8 ms-1 me-1">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">سرپرست مربوطه</h3>
                </div>
                <div class="card-body">
                    <h6 class="text-muted">
                        - {{ $userObject->first_name . ' ' . $userObject->last_name }}
                    </h6>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card ms-3 me-3">
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                            <!--begin::Table head-->
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="min-w-125px">عنوان</th>
                                    <th class="min-w-125px">تعداد</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">

                                <tr>
                                    <td>{{ $helpObject->title }}</td>
                                    <td>

                                        <form
                                            action="{{ route('admin.helps.store.detail', [$needy, $helpObject->id, $userObject->id, $entity]) }}"
                                            method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        @include('admin.__components.input-text', [
                                                            'name' => 'unit',
                                                            'placeholder' => 'تعداد',
                                                        ])
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select class="form-control select picker" id="kt_select2_commodity" name="commodity">
                                                            @foreach ($commodities as $item)
                                                                <option value="{{ $item['id'] }}"
                                                                    input-stock="{{ $item['stock'] }}"
                                                                    url="{{ route('admin.helps.create.detail.commodity', [$needy, $item['id']]) }}">
                                                                    {{ $item['title'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                        @error('commodity')
                                                            <p class="text-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-lg form-control-solid"
                                                            id="input_stock" type="text" name="stock"
                                                            placeholder="موجودی" value="{{ old('stock') }}"
                                                            autocomplete="off" />

                                                        <p id="stock" class="validation-error-msg text-danger"
                                                            style="display: none"></p>
                                                        @error('stock')
                                                            <p class="text-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="ms-19 mt-2">
                                                        <button type="submit" class="btn btn-sm btn-primary ">
                                                            <span class="">ثبت</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end::Table-->
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            @include('admin.helps.all-detail')
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin-assets/plugins/added/sweetalert/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $('.delete_helpDetail').click(function(e) {
            e.preventDefault();

            // console.log('delete blog click !');
            console.log('helpDetail id : ' + $(this).data('helpDetail-id'));

            let helpDetail_id = $(this).data('helpDetail-id');

            let url =
                "{{ route('admin.helps.delete.detail', ['needy' => $needy, $helpObject->id, $userObject->id, $entity, ':helpDetail-id']) }}";
            console.log(url);
            url = url.replace(':helpDetail-id', helpDetail_id)
            console.log(url);

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

        // set session to message(sweetalert) is shown `only once` -> flash session
        @if (session('success'))
            Swal.fire({
                icon: 'success',
                title: 'عملیات موفق',
                text: "{{ session('success') }}", // session('success') = with('success', '...')
                confirmButtonText: 'باشه'
            })
        @endif
    </script>

    <script>
        $('#kt_select2_commodity').on('change', function() {

            var selectBox = document.getElementById("kt_select2_commodity");
            var selectedOption = selectBox.options[selectBox.selectedIndex];

            console.log(selectedOption);
            console.log(selectedOption.getAttribute("input-stock"));

            // روش اول برای گرفتن موجودی قلم مورد نظر
            $("#input_stock").val(selectedOption.getAttribute("input-stock"));


            // روش دوم برای گرفتن موجودی قلم مورد نظر
            $.ajax({
                url: selectedOption.getAttribute("url"),
                type: "GET",
                success: function(data_res) {
                    console.log(data_res);
                    console.log(data_res.stock);
                }
            });
            // $( "#input_stock" ).text(ww);
        });


        // $(document).ready(function () {
        //     function ChangeDrop() {
        //         alert('vhj');
        //         var item = $('#kt_select2_commodity :selected').text();
        //         $("#input_stock").val(item);
        //     }
        // });

        // $("#kt_select2_commodity").on("change", function () {
        //     $("#input_stock").val($(this).find("option:selected").text());
        // });
    </script>
@endsection
