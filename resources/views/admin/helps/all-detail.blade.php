<div class="card ms-3 me-3 mt-5">
    <div class="app-container container-xxl d-flex flex-stack py-3">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-4">
                فهرست
            </h1>
            <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                 تعداد کل :
                    <i class="badge badge-sm badge-light-warning px-2"
                       style="font-size: 10px">{{ $help_details->count() }}</i>
                </span>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3">

        </div>
    </div>
    <div class="card-body py-4">
        <!--begin::Table-->
        <div class="table-responsive">
            <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                <!--begin::Table head-->
                <thead>
                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                    <th class="min-w-125px">آی دی</th>
                    <th class="min-w-125px">نوع کمک</th>
                    <th class="min-w-100px">مقدار</th>
                    <th class="min-w-125px">تنظیمات</th>
                </tr>
                </thead>
                <tbody class="text-gray-600 fw-semibold">
                @foreach($help_details as $help_detail)
                    <tr>
                        <td>{{ $help_detail->id }}</td>
                        <td>{{ $help_detail->entity_type }}</td>
                        <td>{{ $help_detail->detail }}</td>
                        <td>

                            <a href="javascript:;"
                               class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                               data-inbox="dismiss" data-bs-toggle="modal" data-bs-target="#kt_modal_new_card_{{ $help_detail->id }}" title="ویرایش">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-pencil-alt"></i>
                                    </span>
                                <!--end::Svg Icon-->
                            </a>

                            <a href="{{ route('admin.helps.delete.detail',['needy' => $needy,$helpObject->id, $userObject->id, $entity,$help_detail]) }}"
                            class="btn btn-sm btn-icon btn-light-danger btn-active-danger" title="حذف">
                                <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-trash"></i>
                                </span>
                            </a>
{{--                            <a href="javascript:;"--}}
{{--                               data-helpDetail-id="{{$help_detail->id}}"--}}
{{--                               class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_helpDetail"--}}
{{--                               data-inbox="dismiss" data-toggle="tooltip" title="حذف">--}}
{{--                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->--}}
{{--                                <span class="svg-icon svg-icon-2 p-1">--}}
{{--                                        <i class="fa fa-trash"></i>--}}
{{--                                </span>--}}
{{--                                <!--end::Svg Icon-->--}}
{{--                            </a>--}}
                        </td>
                        <!--end::Action=-->
                    </tr>

                    <div class="modal fade" id="kt_modal_new_card_{{ $help_detail->id }}" tabindex="-1" style="display: none;"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>ویرایش کمک</h4>

                                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                        <span class="svg-icon svg-icon-1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                                      transform="rotate(-45 6 17.3137)" fill="currentColor">

                                                </rect>
                                                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                                      transform="rotate(45 7.41422 6)" fill="currentColor">
                                                </rect>
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                                <!--end::Modal header-->
                                <!--begin::Modal body-->
                                <div class="modal-body scroll-y">
                                    <!--begin::Form-->
                                    <form action="{{ route('admin.helps.update.detail', [$needy, $helpObject->id, $userObject->id, $entity, $help_detail]) }}" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    @include('admin.__components.label', ['title' => 'تعداد', 'required'=> 1])
                                                    @include('admin.__components.input-text', [
                                                             'name' => 'unit',
                                                             'placeholder' => 'تعداد',
                                                             'value' => $help_detail->detail
                                                             ])
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    @include('admin.__components.label', ['title' => 'اقلام مربوطه', 'required'=> 1])
                                                    @include('admin.__components.select-2', [
                                                             'name' => 'commodity',
                                                             'items' => $commodities,
                                                             'selectedItem' => $help_detail->entity_id
                                                             ])
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center pt-15">
                                            <button type="submit" id="kt_modal_new_card_submit" class="btn btn-sm btn-light-primary">
                                                <span class="indicator-label">ثبت تغییرات</span>
                                                <span class="indicator-progress">Please wait...
                                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                            </button>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>

                            </div>
                            <!--end::Modal content-->
                        </div>
                        <!--end::Modal dialog-->
                    </div>

                @endforeach
                </tbody>
            </table>
        </div>
        <!--end::Table-->
    </div>

</div>

