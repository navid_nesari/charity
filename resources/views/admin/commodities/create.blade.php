@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/plugins/custom/jstree/jstree.bundle.rtl.css')}}">
@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت کالا'])
@endsection

@section('content')
    <form action="{{ route('admin.commodities.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card ms-3">
                    <div class="card-header">
                        <h3 class="card-title">ثبت کالای جدید</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group mb-8">
                            @include('admin.__components.label', ['title' => 'عنوان', 'required' => 1])
                            @include('admin.__components.input-text', [
                                     'name' => 'title',
                                     'placeholder' => 'عنوان'
                                     ])
                        </div>
                        <div class="form-group mb-8">
                            @include('admin.__components.label', ['title' => 'موجودی', 'required' => 1])
                            @include('admin.__components.input-text', [
                                     'name' => 'stock',
                                     'placeholder' => 'موجودی'
                                     ])
                        </div>
                        <div class="form-group mb-8">
                            @include('admin.__components.label', ['title' => 'تعداد', 'required' => 1])
                            @include('admin.__components.input-text', [
                                     'name' => 'unit',
                                     'placeholder' => 'تعداد'
                                     ])
                        </div>
                        <div class="form-group">
                            @include('admin.__components.label', ['title' => 'توضیحات'])
                            @include('admin.__components.textarea', ['name' => 'description'])
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-flush py-4 me-2">
                    <div class="card-header">
                        <div class="card-title">
                            <h4>وضعیت</h4>
                        </div>
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <div class="rounded-circle bg-success w-15px h-15px"
                                 id="kt_ecommerce_add_category_status"></div>
                        </div>
                        <!--end::Card toolbar-->

                    </div>
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Select2-->
                        <div class="fv-row fv-plugins-icon-container">

                            @include('admin.__components.horizontal-radiobutton',[
                                    'items' => $statuses,
                                    'name' => 'status',
                                    'activeKey' => \App\Constants\Constant::ACTIVE,
                            ])

                        </div>
                        <!--end::Datepicker-->
                    </div>
                    <!--end::Card body-->
                </div>

                <div class="card card-flush mt-4">
                    <div class="card-header">
                        <div class="card-title">
                            <h4>دسته بندی</h4>
                        </div>
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <div class="rounded-circle bg-success w-15px h-15px"
                                 id="kt_ecommerce_add_category_status"></div>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Select2-->
                        <div class="fv-row fv-plugins-icon-container">

                            @include('admin.__components.jsTree',[
                                    'name' => 'categories',
                                    'items' => $categories,
                                    'isMultiple' => true
                            ])

                        </div>
                        <!--end::Datepicker-->
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{asset('admin-assets/plugins/custom/jstree/jstree.bundle.js')}}"></script>
@endsection