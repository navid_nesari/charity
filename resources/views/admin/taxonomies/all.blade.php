@extends('layouts.master')

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'کمک ها'])
@endsection

@section('content')

    <div class="row">
        <div class="col-4">
            @include('admin.taxonomies.create')
        </div>
        <div class="col-8">
            <div class="card ms-3 me-3">
                <div class="app-container container-xxl d-flex flex-stack py-3">
                    <!--begin::Page title-->
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-4">
                            فهرست
                        </h1>
                        <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                 تعداد کل :
                    <i class="badge badge-sm badge-light-warning px-2"
                       style="font-size: 10px">{{ $taxonomies->count() }}</i>
                </span>
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">

                    </div>
                </div>
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                            <!--begin::Table head-->
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="min-w-125px">عنوان</th>
                                <th class="min-w-200px">توضیحات</th>
                                <th class="w-100px">وضعیت</th>
                                <th class="w-125px">تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @foreach($taxonomies as $taxonomy)
                                <tr>
                                    <td>{{ $taxonomy->title }}</td>
                                    <td>{{ $taxonomy->description }}</td>
                                    <td>{!! $taxonomy->webPresent()->status !!}</td>
                                    <td>
                                        <a href="{{ route('admin.taxonomies.edit', ['entity'=> $entity, $taxonomy]) }}"
                                           class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                           data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                                <i class="fa fa-pencil-alt"></i>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>


                                        <a href="javascript:;"
                                           data-taxonomy-id="{{$taxonomy->id}}"
                                           class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_taxonomy"
                                           data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                            <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                    </td>
                                    <!--end::Action=-->
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end::Table-->
                </div>

            </div>
        </div>

    </div>

@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/added/sweetalert/sweetalert.min.css') }}" />
@endsection

@section('script')

    <script src="{{ asset('admin-assets/plugins/added/sweetalert/sweetalert2.min.js') }}" ></script>

    <script type="text/javascript">
        $('.delete_taxonomy').click(function (e) {
            e.preventDefault();

            // console.log('delete blog click !');
            console.log('taxonomy id : ' + $(this).data('taxonomy-id'));

            let taxonomy_id = $(this).data('taxonomy-id');

            let url = "{{ route('admin.taxonomies.delete',['entity' => $entity, ':taxonomy-id'])}}";
            console.log(url);
            url = url.replace(':taxonomy-id', taxonomy_id)
            console.log(url);

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

        // set session to message(sweetalert) is shown `only once` -> flash session
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>

@endsection


