@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"
          integrity="sha512-gOQQLjHRpD3/SEOtalVq50iDn4opLVup2TF8c4QPI3/NmUPNZOk2FG0ihi8oCU/qYEsw4P6nuEZT2lAG0UNYaw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'مددجویان'])
@endsection

@section('content')
    <div class="card ms-3 me-3">
        <div class="app-container container-xxl d-flex flex-stack py-3">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-4">
                    فهرست مددجویان
                </h1>
                <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                 تعداد کل :
                    <i class="badge badge-sm badge-light-warning px-2"
                       style="font-size: 10px">{{ $needies->count() }}</i>
                </span>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">
                <a href="{{ route('admin.needies.create') }}" class="btn btn-sm fw-bold btn-primary">
                    ثبت اعضای خانواده
                </a>
            </div>

        </div>
        <div class="card-body py-4">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                    <!--begin::Table head-->
                    <thead>
                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                        <th class="min-w-200px sorting">نام و نام خانوادگی</th>
                        <th class="min-w-125px sorting">تاریخ تایید</th>
                        <th class="min-w-150px sorting">تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">

                    @if(isset($needies['root']) && $needies['root']->count() > 0)

                        @include('admin.needies.family-head.list', ['items' => $needies['root'] ])

                    @endif

                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>

    </div>
@endsection

