@foreach($items as $item)

    @include('admin.needies.family-head.item', ['item' => $item])

    <!-- $item->id is ID of parent so $needies[parent-id] is a child -->
    @if(isset($needies[$item->id]))

        @foreach($needies[$item->id] as $childItem)

            @include('admin.needies.family-head.item', ['item' => $childItem, 'isChild' => true])

        @endforeach

    @endif


@endforeach