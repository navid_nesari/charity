@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/persian-datepicker.min.css')}}"/>
@endsection

@section('content')

        <div class="row mt-4">
            <form class="d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.needies.families.member.store', $needy) }}" method="post"
                  enctype="multipart/form-data">
                @csrf
            <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-4" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>افزودن نیازمند</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{ route('admin.needies.families.head.all') }}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'نام', 'required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'first_name',
                                                'placeholder' => 'نام',
                                                ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'نام خانوادگی', 'required' => 1])
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        @include('admin.__components.input-text', [
                                                'name' => 'last_name',
                                                'placeholder' => 'نام خانوادگی',
                                                ])
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'شماره موبایل'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'mobile',
                                            'placeholder' => 'شماره موبایل'
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'تاریخ تولد'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.datepicker', [
                                            'name' => 'birth_date',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'تاریخ تایید'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.datepicker', [
                                            'name' => 'approved_date',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'کد ملی','required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'code_melli',
                                            'placeholder' => 'کد ملی',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'آدرس'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.textarea', [
                                            'name' => 'address',
                                            'placeholder' => 'آدرس',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <div class="d-flex justify-content-start ms-1">
                        <!--begin::Button-->
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت مدیر</span>

                        </button>
                        <!--end::Button-->
                    </div>
                </div>
                <!--end::Main column-->
                <!--begin::Aside column-->
                <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-3 me-lg-4">
                    <!--begin::Status-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>وضعیت</h4>
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar">
                                <div class="rounded-circle bg-success w-15px h-15px"
                                     id="kt_ecommerce_add_category_status"></div>
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            <div class="fv-row fv-plugins-icon-container">

                                @include('admin.__components.horizontal-radiobutton',[
                                        'items' => $statuses,
                                        'name' => 'status',
                                        'activeKey' => \App\Constants\Constant::ACTIVE,
                                ])

                            </div>

                            <!--end::Datepicker-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->
                    <!--begin::Thumbnail settings-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>تصویر</h4>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <div class="fv-row fv-plugins-icon-container">

                                @include('admin.__components.image-input',[
                                    'name' => 'avatar',
                                    ])

                            </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Thumbnail settings-->
                </div>
            </form>
        </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"
            integrity="sha512-7VTiy9AhpazBeKQAlhaLRUk+kAMAb8oczljuyJHPsVPWox/QIXDFOnT9DUk1UC8EbnHKRdQowT7sOBe7LAjajQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>

    <script src="{{asset('admin-assets/js/persian-datepicker.min.js')}}"></script>

    <script src="{{asset('admin-assets/js/persian-date.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $("#investigate_date").pDatepicker({
                initialValue : true,
                format: 'L'
            });

        });

    </script>
@endsection
