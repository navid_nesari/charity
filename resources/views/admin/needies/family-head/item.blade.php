<!--On this page, we have two values for the $item variable : $item =$needies['root'] & $item = $childItem -->
<tr>
    <td>
        @if(isset($isChild) && $isChild)
           --
        @endif
        {{ $item->user->first_name }}
    </td>
    <td>{{ jdate($item->approved_date)->format('Y/m/d') }}</td>
    <td>

        @if(is_null($item->parent_id))
            <a href="{{ route('admin.needies.families.member.create', $item) }}"
               class="btn btn-sm btn-light-primary btn-active-primary">
                       افزودن عضو خانوار جدید
            </a>

            <a href="{{ route('admin.helps.all', ['needy' => $item]) }}"
               class="btn btn-sm btn-light-primary btn-active-primary">
                 کمک ها
            </a>
        @endif
        <a href="{{ route('admin.needies.families.member.edit', $item) }}"
           class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
           data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">

            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
            <span class="svg-icon svg-icon-2 p-1">
                <i class="fa fa-user-edit"></i>
            </span>
            <!--end::Svg Icon-->
        </a>


        <a href="javascript:;"
           data-item-id="{{$item->id}}"
           class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_item"
           data-inbox="dismiss" data-toggle="tooltip" title="حذف">
            <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
            <span class="svg-icon svg-icon-2 p-1">
                <i class="fa fa-trash"></i>
            </span>
            <!--end::Svg Icon-->
        </a>
    </td>
    <!--end::Action=-->
</tr>

@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"
            integrity="sha512-7VTiy9AhpazBeKQAlhaLRUk+kAMAb8oczljuyJHPsVPWox/QIXDFOnT9DUk1UC8EbnHKRdQowT7sOBe7LAjajQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>

    <script type="text/javascript">
        $('.delete_item').click(function (e) {
            e.preventDefault();

            // console.log('delete blog click !');
            console.log('item id : ' + $(this).data('item-id'));

            let item_id = $(this).data('item-id');

            let url = "{{ route('admin.needies.delete',':item-id') }}";
            // console.log(url);
            url = url.replace(':item-id', item_id)
            // console.log(url);

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد!',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

    </script>
@endsection